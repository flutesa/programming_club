def have_unsafe_pos(a):
    for i in range(8):
        for j in range(8):
            if a[i] == a[j] and i != j and a[i] != 0:
                return "YES"
            elif a[i] + i == a[j] + j and i != j and a[i] != 0 and a[j] != 0:
                return "YES"
            elif a[i] - i == a[j] - j and i != j and a[i] != 0 and a[j] != 0:
                return "YES"
    return "NO"

array = []

for k in range(8):
    row_input = list(map(int, input().split(" ")))
    array.append(row_input[1])

print(have_unsafe_pos(array))



