def elka(height):
    counter = 1
    otstup_first = (height*2 - 1) // 2
    for i in range(0, height):
        counter += 1
        for j in range(1, counter):
            if j == counter - 1 and i == height - 1:
                print((j*2 - 1)*"*")
                break
            else:
                print((otstup_first - (j*2 - j))*" ", (j*2 - 1)*"*")


elka(int(input()))
