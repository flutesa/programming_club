def half_el(height):
    counter = 2
    for i in range(0, height):
        counter += 1
        for j in range(1, counter):
            print(j*"*")


def half_el_v2(height):
    counter = 1
    for i in range(0, height):
        counter += 1
        for j in range(1, counter):
            print((j*2 - 1)*"*")


def elka_without_first_star(height):
    counter = 2
    otstup_first = ((height+1)*2 - 1) // 2
    for i in range(1, height+1):
        counter += 1
        for j in range(1, counter):
            if j == counter - 1 and i == height:
                print((j*2 - 1)*"*")
                break
            else:
                print((otstup_first - (j*2 - j))*" ", (j*2 - 1)*"*")



for i in range(0, 11):
    print(i)
    elka_without_first_star(i)
    print("__________________")
    print("\n")